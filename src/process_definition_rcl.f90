!#####################################################################
!!
!!  File  process_definition_rcl.f90
!!  is part of RECOLA (REcursive Computation of One Loop Amplitudes)
!!
!!  Copyright (C) 2015-2019   Stefano Actis, Ansgar Denner,
!!                            Lars Hofer, Jean-Nicolas Lang,
!!                            Andreas Scharf, Sandro Uccirati
!!
!!  RECOLA is licenced under the GNU GPL version 3,
!!         see COPYING for details.
!!
!#####################################################################

  module process_definition_rcl
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  use input_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  implicit none

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  contains

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine define_process_rcl (npr,processIn,order)

  ! With this subroutine the user can define the processes to be
  ! computed.

  ! "npr" is the process number the user wants to assign to process
  ! described by "processIn". If the subroutine is called more than
  ! once, each process "processIn" must have a different process
  ! number "npr".

  ! "order" is the loop-order at which we want to compute the process
  ! "processIn". It is a character variable which can take only the
  ! values 'LO' and 'NLO'.

  ! "processIn" is a character variable which defines the process.

  ! "processIn" is a list of particles separated by "->". The
  ! incoming (outgoing) particles are on the left (right) side of
  ! "->".
  ! Any number of incoming and outgoing particles is allowed.
  ! The symbols for the particles are:
  ! Scalars:       'H', 'p0', 'p+', 'p-'
  ! Vector bosons: 'g', 'A', 'Z', 'W+', 'W-'
  ! leptons:       'nu_e', 'nu_e~', 'e-', 'e+',
  !                'nu_mu', 'nu_mu~', 'mu-', 'mu+',
  !                'nu_tau', 'nu_tau~', 'tau-', 'tau+'
  ! quarks:        'u', 'u~', 'd', 'd~',
  !                'c', 'c~', 's', 's~',
  !                't', 't~', 'b', 'b~'
  ! All these symbols (particles and "->") must be separated by at
  ! least one blank character.
  ! Examples of allowed values for "processIn":
  ! 'e+ e- -> mu+ mu-'
  ! 'u u~ -> W+ W- g'
  ! 'u d~ -> W+ g g g'
  ! 'u  g  ->  u  g  Z'
  ! 'u    g  -> u        g  tau- tau+'

  ! Additional symbols for helicities are allowed.
  ! The symbols "[-]", "[+]" and "[0]" account for a specific
  ! polarization of a particle.
  ! For fermions only "[-]" and "[+]" are allowed and they represent
  ! left-handed and right-handed fermions respectively.
  ! For massless vector bosons only "[-]" and "[+]" are allowed and
  ! they represent respectively the -1 and +1 transverse
  ! polarizations.
  ! For massive vector bosons "[-]", "[+]" and "[0]" are allowed and
  ! they represent respectively -1, +1 transverse polarizations and
  ! longitudinal polarization.
  ! The symbols "[-]", "[+]", "[0]" must follow the particle in the
  ! character "processIn".
  ! Blank characters can separate "[-],[+],[0]" from the particle.
  ! Examples:
  ! 'e+[+] e- [-] -> Z H':
  ! e+ is right-handed, e- is left-handed, Z is unpolarized.
  ! 'u u~ -> W+[-] W-[+]':
  ! u and u~ are unpolarised, while W+ and W- are transverse

  ! Contributions with specific intermediate states can be selected,
  ! where intermediate states are particles decaying into any number
  ! of other particles.  To this end, in the process declaration the
  ! decaying particle must be followed by round brackets "( ... )"
  ! containing the decay products.
  ! Multiple and nested decays are allowed.
  ! Blank characters can separate "(", ")" and the particles.
  ! Examples:
  ! 'e+ e- -> W+ W-(e- nu_e~)'
  ! 'e+ e- -> Z H ( b~[+] b[-] )'
  ! 'e+ e- -> t( W+(u d~) b) t~(e- nu_e~ b~)'
  ! 'u  u~ -> Z ( mu+(e+ nu_e nu_mu~) mu-(e- nu_e~ nu_mu) ) H'


  integer,          intent(in) :: npr
  character(len=*), intent(in) :: processIn,order

  integer                :: i,j,n,n0,n1,n2,le,leIn,l0,lmax,sign,nd0, &
                            nd,lend(99),pond(99),binnd(99),polegs(99)
  integer, allocatable   :: inprT(:),legsInT(:),legsOutT(:),         &
                            parT(:,:),helT(:,:),resMaxT(:),          &
                            binResT(:,:),parResT(:,:),powgsT(:,:,:), &
                            pa(:),paT(:),he(:),heT(:)
  logical                :: opennd(99)
  logical, allocatable   :: loopT(:),prexistsT(:)
  integer, allocatable   :: qflowT(:,:),polprojT(:,:)
  integer, parameter     :: nopol=-2
  character              :: cpr*99,cpr0*99,che*3
  character, allocatable :: cpa(:)*7,cpaT(:)*7,processT(:)*99

  if (processes_generated) then
    if (warnings(401).le.warning_limit) then
      warnings(401) = warnings(401) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR 401: Processes already generated.'
      write(nx,*) '           define_process_rcl can not be called ', &
                         'at this point'
      write(nx,*)
      call toomanywarnings(401)
    endif
  endif

  do i = 1,prTot
    if (inpr(i).eq.npr) then
      if (warnings(402).le.warning_limit) then
        warnings(402) = warnings(402) + 1
        call openOutput
        write(nx,*)
        write(nx,'(1x,a,i3,a)') 'ERROR 402: Index ',npr,' has been already used'
        write(nx,*)             '           for the definition of another process.'
        write(nx,*)             '           Definition of process ', &
                                        trim(adjustl(processIn)), &
                                      ' has failed.'
        write(nx,*)
        call toomanywarnings(402)
      endif
      call istop (ifail,1)
      return
    endif
  enddo

  cpr = adjustl(processIn)


  n = 99
  do while (index (cpr,' [').ne.0)
    n = index (cpr,' [')
    cpr = cpr(1:n-1)//cpr(n+1:)
  enddo
  n = 99
  do while (scan(cpr,'(').ne.0)
    n = scan(cpr,'(')
    cpr = cpr(1:n-1)//' { '//cpr(n+1:)
  enddo
  n = 99
  do while (scan(cpr,')').ne.0)
    n = scan(cpr,')')
    cpr = cpr(1:n-1)//' } '//cpr(n+1:)
  enddo


  le = 0
  leIn = 0

  pond = 0
  nd0 = 0
  nd = 0

  do while (cpr.ne.'')


    if (le.gt.0) then
      allocate (cpaT(le)); cpaT(:le) = cpa(:le); deallocate (cpa)
      allocate ( paT(le));  paT(:le) =  pa(:le); deallocate ( pa)
      allocate ( heT(le));  heT(:le) =  he(:le); deallocate ( he)
    endif
    le = le + 1
    allocate (cpa(le),pa(le),he(le))
    if (le.gt.1) then
      cpa(:le-1) = cpaT(:le-1); deallocate (cpaT)
       pa(:le-1) =  paT(:le-1); deallocate ( paT)
       he(:le-1) =  heT(:le-1); deallocate ( heT)
    endif

    n0 = scan(cpr,'>')
    n1 = scan(cpr,' ') - 1
    sign = 1
    if (n1.ge.n0) then
      cpr = adjustl(cpr(n0+1:))
      n1 = scan(cpr,' ') - 1
      sign = -1
    else
      leIn = le
    endif


    n2 = scan(cpr(1:n1),'[')
    if (n2.eq.0) then
      cpa(le) = cpr(1:n1);        he(le) = 111
    else
      che = cpr(n2:n2+2)
      if     (che.eq.'[-]') then; he(le) = -1 * sign
      elseif (che.eq.'[0]') then; he(le) =  0
      elseif (che.eq.'[+]') then; he(le) = +1 * sign
      else;                       he(le) = 111
      endif
      cpa(le) = cpr(1:n2-1)
    endif

    cpr0 = adjustl(cpr(n1+1:))
    if (cpr0(1:1).eq.'{'.and.he(le).ne.111) then
      if (warnings(403).le.warning_limit) then
        warnings(403) = warnings(403) + 1
        call openOutput
        write(nx,*)
        write(nx,'(/,BN,a,i4,4a,/)') &
        'WARNING 403 in process with index ',npr,': ', &
        'intermediate particle ',trim(cpa(le)),    &
        ' can not have a specific helicity'
        write(nx,*)
        call toomanywarnings(403)
      endif
      he(le) = 111
    endif

    pa(le) = npar(cpa(le))
    if (sign.eq.-1) pa(le) = anti(pa(le))

    cpr = adjustl(cpr(n1+1:))

    if (cpr(1:1).eq.'{') then
      nd0 = nd0 + 1
      nd  = nd0
      opennd(nd) = .true.
      pond(nd) = pa(le)
      lend(nd) = le
      le = le - 1
      cpr = adjustl(cpr(2:))
    endif

    do while (cpr(1:1).eq.'}')
      polegs(nd) = le - lend(nd) + 1
      cpr = adjustl(cpr(2:))
      binnd(nd) = 2**(lend(nd)-1)*( 2**polegs(nd) - 1 )
      opennd(nd) = .false.
      nd = nd - 1
      do i = nd0,1,-1
        if (opennd(i)) then
          nd = i
          exit
        endif
      enddo
    enddo

  enddo

  l0 = 0
  if (prTot.gt.0) then
    l0 = maxval(legsIn+legsOut)
    allocate (processT(            prTot));  processT = process
    allocate (   inprT(            prTot));     inprT =    inpr
    allocate ( legsInT(            prTot));   legsInT =  legsIn
    allocate (legsOutT(            prTot));  legsOutT = legsOut
    allocate (    parT(l0,         prTot));      parT =     par
    allocate (    helT(l0,         prTot));      helT =     hel
    allocate ( resMaxT(            prTot));   resMaxT =  resMax
    allocate ( binResT(l0,         prTot));   binResT =  binRes
    allocate ( parResT(l0,         prTot));   parResT =  parRes
    allocate (   loopT(            prTot));     loopT =    loop
    allocate (  qflowT(l0,         prTot));    qflowT =    qflow
    allocate (polprojT(2**(l0-1)-1,prTot));  polprojT = polproj
    allocate (  powgsT(0:l0,0:1,   prTot));    powgsT =   powgs
    allocate (prexistsT(           prTot)); prexistsT = prexists
    deallocate (process,inpr,legsIn,legsOut,par,hel,resMax,binRes, &
                parRes,loop,qflow,polproj,powgs,prexists)
  endif

  prTot = prTot + 1

  lmax = max(l0,le)

  allocate (process(              prTot))
  allocate (   inpr(              prTot));     inpr = 0
  allocate ( legsIn(              prTot));   legsIn = 0
  allocate (legsOut(              prTot));  legsOut = 0
  allocate (    par(lmax,         prTot));      par = 0
  allocate (    hel(lmax,         prTot));      hel = 0
  allocate ( resMax(              prTot));   resMax = 0
  allocate ( binRes(lmax,         prTot));   binRes = 0
  allocate ( parRes(lmax,         prTot));   parRes = 0
  allocate (   loop(              prTot));     loop = .false.
  allocate (  qflow(lmax,         prTot));    qflow = 0
  allocate (polproj(2**(lmax-1)-1,prTot));  polproj = nopol
  allocate (  powgs(0:lmax,0:1,   prTot));    powgs = 1
  allocate (prexists(             prTot)); prexists = .true.

  if (prTot.gt.1) then
    process(           :prTot-1) = processT
       inpr(           :prTot-1) =    inprT
     legsIn(           :prTot-1) =  legsInT
    legsOut(           :prTot-1) = legsOutT
        par(:l0,       :prTot-1) =     parT
        hel(:l0,       :prTot-1) =     helT
     resMax(           :prTot-1) =  resMaxT
     binRes(:l0,       :prTot-1) =  binResT
     parRes(:l0,       :prTot-1) =  parResT
       loop(           :prTot-1) =    loopT
      qflow(:l0,       :prTot-1) =    qflowT
  polproj(:2**(l0-1)-1,:prTot-1) = polprojT
      powgs(0:l0,0:1,  :prTot-1) =   powgsT
      prexists(        :prTot-1) = prexistsT
    deallocate (processT,inprT,legsInT,legsOutT,parT,helT,resMaxT, &
                binResT,parResT,loopT,qflowT,polprojT,powgsT,prexistsT)
  endif

  process(prTot) = adjustl(processIn)

  inpr(prTot) = npr
  do i = 1,prTot
  do j = 1,prTot
    if (i.ne.j.and.inpr(i).eq.inpr(j)) then
      if (warnings(404).le.warning_limit) then
        warnings(404) = warnings(404) + 1
        call openOutput
        write(nx,*)
        write(nx,*) 'ERROR 404: define_process_rcl called for different '// &
                           'processes with the same process-index'
        write(nx,*)
        call toomanywarnings(404)
      endif
      call istop (ifail,1)
    endif
  enddo
  enddo

  if (leIn.le.0) then
    if (warnings(405).le.warning_limit) then
      warnings(405) = warnings(405) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR 405: define_process_rcl called for a process ', &
                         'without incoming particles'
      write(nx,*)
      call toomanywarnings(405)
    endif
    call istop (ifail,1)
  endif
  if (le-leIn.le.0) then
    if (warnings(406).le.warning_limit) then
      warnings(406) = warnings(406) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR 406: define_process_rcl called for a process ', &
                         'without outgoing particles'
      write(nx,*)
      call toomanywarnings(406)
    endif
    call istop (ifail,1)
  endif

   legsIn(      prTot) = leIn
  legsOut(      prTot) = le - leIn
      par(:le,  prTot) = pa
      hel(:le,  prTot) = he
   resMax(      prTot) = nd0
   binRes(1:nd0,prTot) = binnd(1:nd0)
   parRes(1:nd0,prTot) = pond(1:nd0)

  if     (order.eq.'LO' ) then; loop(prTot) = .false.
  elseif (order.eq.'NLO') then; loop(prTot) = .true.; loopMax = .true.
  else
    if (warnings(407).le.warning_limit) then
      warnings(407) = warnings(407) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR 407: define_process_rcl called at the wrong '// &
                         'loop order '
      write(nx,*) "           (accepted values are order = 'LO','NLO')"
      write(nx,*)
      call toomanywarnings(407)
    endif
    call istop (ifail,1)
  endif

  legsMax = maxval(legsIn+legsOut)

  end subroutine define_process_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
! The next subroutines select/unselect the contributions to the
! amplitude with specific powers of g_s (i.e. the strong coupling:
! g_s^2 = 4*pi*alpha_s).
! If none of them are called all contributions are selected
! (default).
!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine set_gs_power_rcl (npr,gsarray)

  ! This subroutine selects specific powers of g_s, as
  ! specified by the argument "gsarray", for the process with
  ! identifier "npr".
  ! "gsarray(0:,0:1)" is an integer array whose values can be 0 or 1.
  ! The first entry is the power of g_s.
  ! The second entry is the loop order (0 for LO, 1 for NLO)
  ! Example:
  ! process 'd~ d -> u~ u' at NLO
  ! gsarray(0,0) = 1
  ! gsarray(1,0) = 0
  ! gsarray(2,0) = 1
  ! gsarray(0,1) = 0
  ! gsarray(1,1) = 0
  ! gsarray(2,1) = 0
  ! gsarray(3,1) = 0
  ! gsarray(4,1) = 1
  ! Here we select for computation the contributions with g_s^0 and
  ! g_s^2 for the tree amplitude and the g_s^4 contribution for the
  ! loop amplitude. All other contributions are not selected.

  integer, intent(in) :: npr,gsarray(0:,0:)
  integer             :: pr,i,legs

  if (processes_generated) then
    if (warnings(408).le.warning_limit) then
      warnings(408) = warnings(408) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'WARNING 408: Processes already generated.'
      write(nx,*) '             The call of set_gs_power_rcl ', &
                           'for process ',npr,' has no effects.'
      write(nx,*)
      call toomanywarnings(408)
    endif
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings(409).le.warning_limit) then
      warnings(409) = warnings(409) + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR 409: set_gs_power_rcl called with '// &
                                 'undefined process index ',npr
      write(nx,*)
      call toomanywarnings(409)
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)

  do i = 0,legs-2
    if (gsarray(i,0).ne.0.and.gsarray(i,0).ne.1) then
      if (warnings(410).le.warning_limit) then
        warnings(410) = warnings(410) + 1
        call openOutput
        write(nx,*)
        write(nx,*) 'ERROR 410: set_gs_power_rcl called with wrong arguments'
        write(nx,*)
        call toomanywarnings(410)
      endif
      call istop (ifail,1)
    endif
  enddo
  powgs(0:legs-2,0,pr) = gsarray(0:legs-2,0)

  if (loop(pr)) then
    do i = 0,legs
      if (gsarray(i,1).ne.0.and.gsarray(i,1).ne.1) then
        if (warnings(411).le.warning_limit) then
          warnings(411) = warnings(411) + 1
          call openOutput
          write(nx,*)
          write(nx,*) 'ERROR 411: set_gs_power_rcl called with wrong arguments'
          write(nx,*)
          call toomanywarnings(411)
        endif
        call istop (ifail,1)
      endif
    enddo
    powgs(0:legs,1,pr) = gsarray(0:legs,1)
  else
    powgs(0:legs,1,pr) = 0
  endif

  end subroutine set_gs_power_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine select_gs_power_BornAmpl_rcl (npr,gspower)

  ! This subroutine selects the contribution to the Born amplitude
  ! with g_s power "gspower", for the process with process number
  ! "npr" .
  ! The selection of the contributions to the loop amplitude are
  ! uneffected, as well as the selection of the other powers of g_s
  ! for the Born amplitude.

  integer, intent(in)  :: npr,gspower

  integer              :: pr,i,legs

  if (processes_generated) then
    if (warnings(412).le.warning_limit) then
      warnings(412) = warnings(412) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'WARNING 412: Processes already generated.'
      write(nx,*) '             The call of ', &
                           'select_gs_power_BornAmpl_rcl ', &
                           'for process ',npr,' has no effects.'
      write(nx,*)
      call toomanywarnings(412)
    endif
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings(413).le.warning_limit) then
      warnings(413) = warnings(413) + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR 413: select_gs_power_BornAmpl_rcl called '
      write(nx,'(a,i3)') '            with undefined process index ',npr
      write(nx,*)
      call toomanywarnings(413)
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)
  if (gspower.lt.0.or.gspower.gt.legs-2) then
    if (warnings(414).le.warning_limit) then
      warnings(414) = warnings(414) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR 414: select_gs_power_BornAmpl_rcl called with wrong gs power'
      write(nx,*)
      call toomanywarnings(414)
    endif
    call istop (ifail,1)
  endif

  powgs(gspower,0,pr) = 1

  end subroutine select_gs_power_BornAmpl_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine select_gs_power_LoopAmpl_rcl (npr,gspower)

  ! This subroutine selects the contribution to the loop amplitude
  ! with g_s power "gspower" for the process with process number
  ! "npr".
  ! The selection of the contributions to the Born amplitude remains
  ! uneffected, as well as the selection of the other powers of g_s
  ! for the loop amplitude.

  integer, intent(in)  :: npr,gspower

  integer              :: pr,i,legs

  if (processes_generated) then
    if (warnings(415).le.warning_limit) then
      warnings(415) = warnings(415) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'WARNING 415: Processes already generated.'
      write(nx,*) '             The call of ', &
                           'select_gs_power_LoopAmpl_rcl ', &
                           'for process ',npr,' has no effects.'
      write(nx,*)
      call toomanywarnings(415)
    endif
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings(416).le.warning_limit) then
      warnings(416) = warnings(416) + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR 416: select_gs_power_LoopAmpl_rcl called '
      write(nx,'(a,i3)') '            with undefined process index ',npr
      write(nx,*)
      call toomanywarnings(416)
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)
  if (gspower.lt.0.or.gspower.gt.legs) then
    if (warnings(417).le.warning_limit) then
      warnings(417) = warnings(417) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR 417: select_gs_power_LoopAmpl_rcl called with wrong gs power'
      write(nx,*)
      call toomanywarnings(417)
    endif
    call istop (ifail,1)
  endif

  powgs(gspower,1,pr) = 1

  end subroutine select_gs_power_LoopAmpl_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine unselect_gs_power_BornAmpl_rcl (npr,gspower)

  ! This subroutine unselects the contribution to the Born amplitude
  ! with g_s power "gspower" for the process with process number
  ! "npr".
  ! The selection of the contributions to the loop amplitude remains
  ! uneffected, as well as the selection of the other powers of g_s
  ! for the Born amplitude.

  integer, intent(in)  :: npr,gspower

  integer              :: pr,i,legs

  if (processes_generated) then
    if (warnings(418).le.warning_limit) then
      warnings(418) = warnings(418) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'WARNING 418: Processes already generated.'
      write(nx,*) '             The call of ', &
                           'unselect_gs_power_BornAmpl_rcl ', &
                           'for process ',npr,' has no effects.'
      write(nx,*)
      call toomanywarnings(418)
    endif
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings(419).le.warning_limit) then
      warnings(419) = warnings(419) + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR 419: unselect_gs_power_BornAmpl_rcl called '
      write(nx,'(a,i3)') '            with undefined process index ',npr
      write(nx,*)
      call toomanywarnings(419)
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)
  if (gspower.lt.0.or.gspower.gt.legs-2) then
    if (warnings(420).le.warning_limit) then
      warnings(420) = warnings(420) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR 420: unselect_gs_power_BornAmpl_rcl called with wrong gs power'
      write(nx,*)
      call toomanywarnings(420)
    endif
    call istop (ifail,1)
  endif

  powgs(gspower,0,pr) = 0

  end subroutine unselect_gs_power_BornAmpl_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine unselect_gs_power_LoopAmpl_rcl (npr,gspower)

  ! This subroutine unselects the contribution to the loop amplitude
  ! with g_s power "gspower" for the process with process number
  ! "npr".
  ! The selection of the contributions to the Born amplitude remains
  ! uneffected, as well as the selection of the other powers of g_s
  ! for the loop amplitude.

  integer, intent(in)  :: npr,gspower

  integer              :: pr,i,legs

  if (processes_generated) then
    if (warnings(421).le.warning_limit) then
      warnings(421) = warnings(421) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'WARNING 421: Processes already generated.'
      write(nx,*) '             The call of ', &
                           'unselect_gs_power_LoopAmpl_rcl ', &
                           'for process ',npr,' has no effects.'
      write(nx,*)
      call toomanywarnings(421)
    endif
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings(422).le.warning_limit) then
      warnings(422) = warnings(422) + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR 422: unselect_gs_power_LoopAmpl_rcl called '
      write(nx,'(a,i3)') '            with undefined process index ',npr
      write(nx,*)
      call toomanywarnings(422)
    endif
    call istop (ifail,1)
  endif

  legs = legsIn(pr) + legsOut(pr)
  if (gspower.lt.0.or.gspower.gt.legs) then
    if (warnings(423).le.warning_limit) then
      warnings(423) = warnings(423) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR 423: unselect_gs_power_LoopAmpl_rcl called with wrong gs power'
      write(nx,*)
      call toomanywarnings(423)
    endif
    call istop (ifail,1)
  endif

  powgs(gspower,1,pr) = 0

  end subroutine unselect_gs_power_LoopAmpl_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine select_all_gs_powers_BornAmpl_rcl (npr)

  ! This subroutine selects all contribution to the Born amplitude
  ! (with any g_s power) for the process with process number "npr".
  ! The selection of the contributions to the loop amplitude remains
  ! uneffected.

  integer, intent(in)  :: npr
  integer              :: pr,i

  if (processes_generated) then
    if (warnings(424).le.warning_limit) then
      warnings(424) = warnings(424) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'WARNING 424: Processes already generated.'
      write(nx,*) '             The call of ', &
                           'select_all_gs_powers_BornAmpl_rcl ', &
                           'for process ',npr,' has no effects.'
      write(nx,*)
      call toomanywarnings(424)
    endif
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings(425).le.warning_limit) then
      warnings(425) = warnings(425) + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR 425: select_all_gs_powers_BornAmpl_rcl called '
      write(nx,'(a,i3)') '            with undefined process index ',npr
      write(nx,*)
      call toomanywarnings(425)
    endif
    call istop (ifail,1)
  endif

  powgs(0:,0,pr) = 1

  end subroutine select_all_gs_powers_BornAmpl_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine select_all_gs_powers_LoopAmpl_rcl (npr)

  ! This subroutine selects all contribution to the loop amplitude
  ! (with any g_s power) for the process with process number "npr".
  ! The selection of the contributions to the Born amplitude remains
  ! uneffected.

  integer, intent(in)  :: npr
  integer              :: pr,i

  if (processes_generated) then
    if (warnings(426).le.warning_limit) then
      warnings(426) = warnings(426) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'WARNING 426: Processes already generated.'
      write(nx,*) '             The call of ', &
                           'select_all_gs_powers_LoopAmpl_rcl ', &
                           'for process ',npr,' has no effects.'
      write(nx,*)
      call toomanywarnings(426)
    endif
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings(427).le.warning_limit) then
      warnings(427) = warnings(427) + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR 427: select_all_gs_powers_LoopAmpl_rcl called '
      write(nx,'(a,i3)') '            with undefined process index ',npr
      write(nx,*)
      call toomanywarnings(427)
    endif
    call istop (ifail,1)
  endif

  powgs(0:,1,pr) = 1

  end subroutine select_all_gs_powers_LoopAmpl_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine unselect_all_gs_powers_BornAmpl_rcl (npr)

  ! This subroutine unselects all contribution to the Born amplitude
  ! (with any g_s power) for the process with process number "npr".
  ! The selection of the contributions to the loop amplitude remains
  ! uneffected.

  integer, intent(in)  :: npr
  integer              :: pr,i

  if (processes_generated) then
    if (warnings(428).le.warning_limit) then
      warnings(428) = warnings(428) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'WARNING 428: Processes already generated.'
      write(nx,*) '             The call of ', &
                           'unselect_all_gs_powers_BornAmpl_rcl ', &
                           'for process ',npr,' has no effects.'
      write(nx,*)
      call toomanywarnings(428)
    endif
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings(429).le.warning_limit) then
      warnings(429) = warnings(429) + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR 429: unselect_all_gs_powers_BornAmpl_rcl '
      write(nx,'(a,i3)') '            called with undefined process index ',npr
      write(nx,*)
      call toomanywarnings(429)
    endif
    call istop (ifail,1)
  endif

  powgs(0:,0,pr) = 0

  end subroutine unselect_all_gs_powers_BornAmpl_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine unselect_all_gs_powers_LoopAmpl_rcl (npr)

  ! This subroutine unselects all contribution to the loop amplitude
  ! (with any g_s power) for the process with process number "npr".
  ! The selection of the contributions to the Born amplitude remains
  ! uneffected.

  integer, intent(in)  :: npr
  integer              :: pr,i

  if (processes_generated) then
    if (warnings(430).le.warning_limit) then
      warnings(430) = warnings(430) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'WARNING 430: Processes already generated.'
      write(nx,*) '             The call of ', &
                           'unselect_all_gs_powers_LoopAmpl_rcl ', &
                           'for process ',npr,' has no effects.'
      write(nx,*)
      call toomanywarnings(430)
    endif
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings(431).le.warning_limit) then
      warnings(431) = warnings(431) + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR 431: unselect_all_gs_powers_LoopAmpl_rcl '
      write(nx,'(a,i3)') '            called with undefined process index ',npr
      write(nx,*)
      call toomanywarnings(431)
    endif
    call istop (ifail,1)
  endif

  powgs(0:,1,pr) = 0

  end subroutine unselect_all_gs_powers_LoopAmpl_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine split_collier_cache_rcl (npr,n)

  ! This subroutine splits the cache of collier for process "npr"
  ! in "n" parts.

  integer, intent(in)  :: npr,n
  integer, allocatable :: nCacheTmp(:)
  logical, allocatable :: cacheOnTmp(:)
  integer              :: pr,i

  if (processes_generated) then
    if (warnings(432).le.warning_limit) then
      warnings(432) = warnings(432) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'WARNING 432: Processes already generated.'
      write(nx,*) '             The call of ', &
                           'split_collier_cache_rcl ', &
                           'for process ',npr,' has no effects.'
      write(nx,*)
      call toomanywarnings(432)
    endif
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings(433).le.warning_limit) then
      warnings(433) = warnings(433) + 1
      call openOutput
      write(nx,*)
      write(nx,*)         'ERROR 433: split_collier_cache_rcl '
      write(nx,'(a,i3)') '            called with undefined process index ',npr
      write(nx,*)
      call toomanywarnings(433)
    endif
    call istop (ifail,1)
  endif

  if (n.lt.0) then
    if (warnings(434).le.warning_limit) then
      warnings(434) = warnings(434) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR 434: split_collier_cache_rcl called with wrong arguments'
      write(nx,*)
      call toomanywarnings(434)
    endif
    call istop (ifail,1)
  endif

  if (.not.allocated(nCache)) then
    allocate(nCache(prTot))
    nCache = 1
  else if (size(nCache) .ne. prTot) then
    allocate(nCacheTmp(size(nCache)))
    nCacheTmp(:) = nCache(:)
    deallocate(nCache)
    allocate(nCache(prTot))
    nCache = 1
    nCache(1:size(nCacheTmp)) = nCacheTmp(:)
  endif

  if (.not.allocated(cacheOn)) then
    allocate(cacheOn(1:prTot))
    cacheOn = .true.
  else if (size(cacheOn) .ne. prTot) then
    allocate(cacheOnTmp(size(cacheOn)))
    cacheOnTmp(:) = cacheOn(:)
    deallocate(cacheOn)
    allocate(cacheOn(prTot))
    cacheOn = .true.
    cacheOn(1:size(cacheOnTmp)) = cacheOnTmp(:)
  endif

  if (n.eq.0) then
    cacheOn(pr) = .false.
  else
    nCache(pr) = n
  endif

  end subroutine split_collier_cache_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  end module process_definition_rcl

!#####################################################################



