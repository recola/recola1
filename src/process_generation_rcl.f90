!#####################################################################
!!
!!  File  process_generation_rcl.f90
!!  is part of RECOLA (REcursive Computation of One Loop Amplitudes)
!!
!!  Copyright (C) 2015-2019   Stefano Actis, Ansgar Denner,
!!                            Lars Hofer, Jean-Nicolas Lang,
!!                            Andreas Scharf, Sandro Uccirati
!!
!!  RECOLA is licenced under the GNU GPL version 3,
!!         see COPYING for details.
!!
!#####################################################################

  module process_generation_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  use input_rcl
  use collier_interface_rcl
  use tables_rcl
  use currents_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  implicit none

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  contains

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine generate_processes_rcl

  ! This subroutine generates all defined processes.
  ! It must be called after all processes are defined.

  real(sp)      :: timeGENin,timeGENout
  character(5)  :: regel,regmu,regta,regu,regd,regc,regs,regt,regb
  character(30) :: fmt1,fmt2,fmt3

  call cpu_time (timeGENin)

  ! tables
  call binaries_tables
  if (loopMax) call tensors_tables
  call particles_tables
  call masses_tables
  call couplings_tables
  if (collier_ct) call initialize_collier_ct
  if (loopMax) call counterterms_tables

  if (regf(26).eq.2) then; regel = 'light'; else; regel = ''; endif
  if (regf(27).eq.2) then; regmu = 'light'; else; regmu = ''; endif
  if (regf(28).eq.2) then; regta = 'light'; else; regta = ''; endif
  if (regf(23).eq.2) then; regu  = 'light'; else; regu  = ''; endif
  if (regf(29).eq.2) then; regd  = 'light'; else; regd  = ''; endif
  if (regf(24).eq.2) then; regc  = 'light'; else; regc  = ''; endif
  if (regf(30).eq.2) then; regs  = 'light'; else; regs  = ''; endif
  if (regf(25).eq.2) then; regt  = 'light'; else; regt  = ''; endif
  if (regf(31).eq.2) then; regb  = 'light'; else; regb  = ''; endif

  call openOutput

  write(nx,*)

  write(nx,'(1x,75("-"))')

  fmt1 = '(2x,a,g21.14,12x,a,g21.14)'
  fmt2 = '(2x,a,g21.14,2x,a)'
  fmt3 = '(2x,a,g21.14,2x,a,5x,a,g21.14)'
  write(nx,'(2x,a)') 'Pole masses and widths [GeV]:'
  write(nx,fmt1) 'M_Z   =', mass_z,         'Width_Z   =', width_z
  write(nx,fmt1) 'M_W   =', mass_w,         'Width_W   =', width_w
  write(nx,fmt1) 'M_H   =', mass_h,         'Width_H   =', width_h
  write(nx,fmt2) 'm_e   =', mass_el, regel
  write(nx,fmt3) 'm_mu  =', mass_mu, regmu, 'Width_mu  =', width_mu
  write(nx,fmt3) 'm_tau =', mass_ta, regta, 'Width_tau =', width_ta
  write(nx,fmt2) 'm_u   =', mass_u,  regu
  write(nx,fmt2) 'm_d   =', mass_d,  regd
  write(nx,fmt3) 'm_c   =', mass_c,  regc,  'Width_c   =', width_c
  write(nx,fmt2) 'm_s   =', mass_s,  regs
  write(nx,fmt3) 'm_t   =', mass_t,  regt,  'Width_t   =', width_t
  write(nx,fmt3) 'm_b   =', mass_b,  regb,  'Width_b   =', width_b

  write(nx,'(1x,75("-"))')

  if     (complex_mass_scheme.eq.0) then
    write(nx,*) ' Renormalization done in the on-shell mass scheme'
  elseif (complex_mass_scheme.eq.1) then
    write(nx,*) ' Renormalization done in the complex-mass scheme'
  endif

  write(nx,'(1x,75("-"))')

  if     (ew_reno_scheme.eq.1) then
    write(nx,'(2x,a,7x,a,g21.14, a)') &
    'EW Renormalization Scheme: gfermi','Gf =',gf,' GeV^-2'
    write(nx,'(2x,40x,a,g21.14)') 'alpha_Gf =',alpha
  elseif (ew_reno_scheme.eq.2) then
    write(nx,'(2x,a,7x,a,g21.14)') &
    'EW Renormalization Scheme: alpha0','alpha(0) =',al0
  elseif (ew_reno_scheme.eq.3) then
    write(nx,'(2x,a,7x,a,g21.14)') &
    'EW Renormalization Scheme: alphaZ','alpha(M_Z) =',alZ
  endif

  write(nx,'(1x,75("-"))')

  if (Nfren.eq.-1) then
    write(nx,'(2x,a)') &
    'alpha_s Renormalization Scheme: Variable flavours Scheme'
  else
    write(nx,'(2x,a,i1,a)') &
    'alpha_s Renormalization Scheme: ',Nfren,'-flavours Scheme'
  endif
  write(nx,'(2x,a,g21.14,7x,a,g21.14,a)') &
  'alpha_s(Q) =',als,'Q =',Qren,' GeV'
  if (use_active_qmasses) then
    write(nx,'(2x,a)') 'Quark masses in the running of alpha_s [GeV]:'
    write(nx,'(2x,a,g21.14)') 'm_c =',mq(4)
    write(nx,'(2x,a,g21.14)') 'm_b =',mq(5)
    write(nx,'(2x,a,g21.14)') 'm_t =',mq(6)
  end if
  write(nx,'(1x,75("-"))')

  write(nx,'(2x,a,g21.14,7x,a,g21.14,a)') &
  'Delta_UV   =',DeltaUV,'mu_UV =',muUV,' GeV'

  write(nx,'(2x,a,g21.14)') &
  'Delta_IR^2 =',DeltaIR2
  write(nx,'(2x,a,g21.14,7x,a,g21.14,a)') &
  'Delta_IR   =',DeltaIR,'mu_IR =',muIR,' GeV'

  write(nx,'(1x,75("-"))')

  if (reg_soft.eq.1) then
    write(nx,'(2x,a)') &
    'Dimensional regularization for soft singularities'
  else
    write(nx,'(2x,a)') &
    'Mass regularization for soft singularities'
    write(nx,'(2x,a,g21.14,a)') 'Mass regulator = ',lambda,' GeV'
  endif

  write(nx,'(1x,75("-"))')

  write(nx,*)

  call generate_currents

  call cpu_time (timeGENout)


  timeGEN = timeGENout - timeGENin

  allocate (timeTI(prTot)); timeTI = 0e0
  allocate (timeTC(prTot)); timeTC = 0e0

  ! collier initialization
  if (loopMax) call initialize_collier

  processes_generated = .true.

  if (print_process_generation_summary) then
    call process_generation_summary
    write(nx,*)
  end if

  contains


  subroutine process_generation_summary

    integer np,pr

    call openOutput

    write(nx,'(1x,a)') 'Process generation summary:'
    np = prTot
    if (np .eq. 1) then
      write(nx,'(1x,a)') trim(adjustl(to_str(np))) // ' process defined'
    else
      write(nx,'(1x,a)') trim(adjustl(to_str(np))) // ' processes defined'
    end if

    do pr = 1, np
      write(nx,'(2x,a)') ''
      write(nx,'(2x,a)') "Process " // trim(adjustl(to_str(inpr(pr)))) // &
                         ": "// trim(adjustl(process(pr)))
      write(nx,'(2x,a)') 'Tree currents    = ' // trim(adjustl(to_str(w0Tot(pr))))
      write(nx,'(2x,a)') 'Tree branches    = ' // trim(adjustl(to_str(bm0prTot(pr))))

      if (loop(pr)) then
        write(nx,'(2x,a)') 'Loop currents    = ' // trim(adjustl(to_str(w1Tot(pr))))
        write(nx,'(2x,a)') 'Loop branches    = ' // trim(adjustl(to_str(bm1prTot(pr))))
      endif

      if (loop(pr)) then
        write(nx,'(2x,a)') 'Tensor integrals = ' // trim(adjustl(to_str(tiTot(pr))))
      end if
      write(nx,'(2x,a)') 'Helicities       = ' // trim(adjustl(to_str(cfTot(pr))))
      if (csTot(pr) .ne. pCsTot(pr)) then
        write(nx,'(2x,a)') 'Colourflows      = ' // trim(adjustl(to_str(csTot(pr)))) &
            // ' ('// trim(adjustl(to_str(pCsTot(pr)))) // ' computed)'
      else
        write(nx,'(2x,a)') 'Colourflows      = ' // trim(adjustl(to_str(csTot(pr))))
      end if

    end do
    write(nx,'(1x,75("-"))')

  end subroutine process_generation_summary

  function to_str(i)
    integer, intent(in) :: i
    character(10)       :: to_str
    write(to_str,'(i10)') i
  end function to_str

  end subroutine generate_processes_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  subroutine process_exists_rcl(npr,exists)
  ! returns whether a process with id "npr" exists after genreation.
  integer, intent(in)          :: npr
  logical, intent(out)         :: exists
  integer                      :: i,pr

  if (.not.processes_generated) then
    if (warnings(441).le.warning_limit) then
      warnings(441) = warnings(441) + 1
      call openOutput
      write(nx,*)
      write(nx,*) 'ERROR 441: Call of get_momenta_rcl not allowed:'
      write(nx,*) '           Processes not generated yet.'
      write(nx,*)
      call toomanywarnings(441)
    endif
    call istop (ifail,1)
  endif

  pr = 0
  do i = 1,prTot
    if (inpr(i).eq.npr) then
      pr = i; exit
    endif
  enddo
  if (pr.eq.0) then
    if (warnings(442).le.warning_limit) then
      warnings(442) = warnings(442) + 1
      call openOutput
      write(nx,*)
      write(nx,'(a,i3)') ' ERROR 442: get_momenta_rcl called '// &
                                 'with undefined process index ',npr
      write(nx,*)
      call toomanywarnings(442)
    endif
    call istop (ifail,1)
  endif

  exists = prexists(pr)

  end subroutine process_exists_rcl

!+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

  end module process_generation_rcl

!#####################################################################



