# File: CMakeLists.txt
# Author: Jean-Nicolas Lang
# Description: CMake configure file for tests
# Last Modified: November 27, 2017

set(CMAKE_CTEST_COMMAND ctest) 
add_custom_target(check COMMAND ${CMAKE_CTEST_COMMAND})

include_directories(${COLLIER_INCLUDE_DIR})

if (with_all_smtests)
  add_executable(test1 EXCLUDE_FROM_ALL twoTOtwo_all.f90)
  add_executable(test2 EXCLUDE_FROM_ALL twoTOthree_all.f90)
  add_executable(test3 EXCLUDE_FROM_ALL twoTOfour_all.f90)
  add_executable(helicity_projection EXCLUDE_FROM_ALL helicity_projection_soft_limit.f90)
else()
  add_executable(test1 EXCLUDE_FROM_ALL twoTOtwo.f90  )
  add_executable(test2 EXCLUDE_FROM_ALL twoTOthree.f90 )
  add_executable(test3 EXCLUDE_FROM_ALL twoTOfour.f90 )
  add_executable(helicity_projection EXCLUDE_FROM_ALL helicity_projection_soft_limit.f90)
endif()
target_link_libraries (test1 recola)
target_link_libraries (test2 recola)
target_link_libraries (test3 recola)
target_link_libraries (helicity_projection recola)
add_test(two_to_two_processes test1)
add_test(two_to_three_processes test2)
add_test(two_to_four_processes test3)
add_test(helicity_projection helicity_projection)

include_directories (${COLLIER_INCLUDE_DIR})
add_dependencies(check test1)
add_dependencies(check test2)
add_dependencies(check test3)
add_dependencies(check helicity_projection)
